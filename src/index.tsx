import React from 'react'
import { createRoot } from 'react-dom/client'
import { BrowserRouter } from 'react-router-dom'
import { Elements } from '@stripe/react-stripe-js'

import App from './App'
import { stripePromise } from './utils/stripe/stripe.utils'

import './index.scss'
import {
    ApolloClient,
    ApolloProvider,
    InMemoryCache,
    createHttpLink,
} from '@apollo/client'
import { setContext } from '@apollo/client/link/context'

const httpLink = createHttpLink({
    uri: 'http://localhost:5555/graphql',
})

const authLink = setContext((_, { headers }) => {
    const token = localStorage.getItem('auth')
    const auth = token ? { authorization: `Bearer ${token}` } : null
    return {
        headers: {
            ...headers,
            ...auth,
        },
    }
})

const client = new ApolloClient({
    link: authLink.concat(httpLink),
    cache: new InMemoryCache({}),
    connectToDevTools: true,
})

const root = createRoot(document.getElementById('root')!)
root.render(
    <React.StrictMode>
        <ApolloProvider client={client}>
            <BrowserRouter>
                <Elements stripe={stripePromise}>
                    <App />
                </Elements>
            </BrowserRouter>
        </ApolloProvider>
    </React.StrictMode>
)
