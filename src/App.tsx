import { useEffect } from 'react';
import { Routes, Route } from 'react-router-dom';

import {
  onAuthStateChangedListener,
  createUserDocumentFromAuth,
} from './utils/firebase/firebase.utils';
import Home from './routes/home/home.component';
import Navigation from './routes/navigation/navigation.component';
import Authentication from './routes/authentication/authentication.component';
import Shop from './routes/shop/shop.component';
import Checkout from './routes/checkout/checkout.component';
import { currentUser } from './cache';
import { useApolloClient } from '@apollo/client';
import { GET_CART } from './gql-operations/queries/cart';

const App = () => {
  const client = useApolloClient()
  useEffect(() => {
    const unsubscribe = onAuthStateChangedListener(async (user) => {
      let userData = null
      if (user) {
        userData = await createUserDocumentFromAuth(user);
        const accessToken = await user.getIdToken()
        localStorage.setItem('auth', accessToken)
      } else {
        localStorage.removeItem('auth')
      }

      currentUser(userData || null)
      client.refetchQueries({
        include: [GET_CART],
      })
    });

    return unsubscribe;
  }, []);

  return (
    <Routes>
      <Route path='/' element={<Navigation />}>
        <Route index element={<Home />} />
        <Route path='shop/*' element={<Shop />} />
        <Route path='auth' element={<Authentication />} />
        <Route path='checkout' element={<Checkout />} />
      </Route>
    </Routes>
  );
};

export default App;
