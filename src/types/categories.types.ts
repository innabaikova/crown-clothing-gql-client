import { Key } from 'react'

export type CategoryDirectory = {
    id: Key
    title: string
    imageUrl: string
    route: string
}
