import { CartDataFragment } from "../../gql/graphql";

const cart: CartDataFragment = {
    __typename: 'Cart',
    items: [
        {
            id: '20',
            name: 'Grey Jean Jacket',
            price: 90,
            imageUrl: 'https://i.ibb.co/N71k1ML/grey-jean-jacket.png',
            quantity: 1,
        },
        {
            id: '1',
            name: 'Brown Brim',
            price: 25,
            imageUrl: 'https://i.ibb.co/ZYW3VTp/brown-brim.png',
            quantity: 1,
        },
    ],
    total: {
        quantity: 2,
        price: 115,
    },
}

export {cart}