import { graphql } from '../../gql'

const GET_CATEGORIES = graphql(/* GraphQL */ `
    query getCategories {
        categories {
            title: id
            items {
                id
                imageUrl
                name
                price
            }
        }
    }
`)

const GET_CATEGORY = graphql(`
    query getCategory($categoryId: ID!) {
        category(id: $categoryId) {
            id
            items {
                id
                imageUrl
                name
                price
            }
        }
    }
`)

export { GET_CATEGORIES, GET_CATEGORY }
