import { graphql } from '../../gql'

const GET_CART = graphql(`
    query Cart {
        cart {
            ...CartData
        }
    }
`)

export { GET_CART }
