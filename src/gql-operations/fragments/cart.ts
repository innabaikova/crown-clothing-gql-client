import { graphql } from '../../gql'

const CART_DATA = graphql(`
    fragment CartData on Cart {
        items {
            id
            name
            price
            imageUrl
            quantity
        }
        total {
            quantity
            price
        }
    }
`)

export { CART_DATA }
