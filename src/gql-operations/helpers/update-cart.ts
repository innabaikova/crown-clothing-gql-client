import { ApolloCache } from "@apollo/client"
import { CartQuery } from "../../gql/graphql"
import { GET_CART } from "../queries/cart"

type Cart = CartQuery['cart']
export function updateCart(cache: ApolloCache<any>, cart: Cart) {
    cache.writeQuery({
        query: GET_CART,
        data: { cart }
    })
}