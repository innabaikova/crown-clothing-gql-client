import { graphql } from '../../gql'

const ADD_TO_CART = graphql(`
    mutation AddToCart($productId: ID!, $categoryId: ID) {
        addToCart(productId: $productId, categoryId: $categoryId) {
            ...CartData
        }
    }
`)

const REMOVE_ITEM = graphql(`
    mutation RemoveFromCart($productId: ID!) {
        removeFromCart(productId: $productId) {
            ...CartData
        }
    }
`)

const CLEAR_ITEM = graphql(`
    mutation ClearCartItem($productId: ID!) {
        clearCartItem(productId: $productId) {
            ...CartData
        }
    }
`)

const CLEAR_CART = graphql(`
    mutation ClearCart {
        clearCart {
            ...CartData
        }
    }
`)

export { ADD_TO_CART, REMOVE_ITEM, CLEAR_ITEM, CLEAR_CART }
