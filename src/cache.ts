import { makeVar } from "@apollo/client";
import { UserData } from "./utils/firebase/firebase.utils";

const isCartOpen = makeVar<boolean>(false)
const currentUser = makeVar<UserData | null>(null)

export {
    isCartOpen,
    currentUser
}