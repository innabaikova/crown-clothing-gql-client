import { useQuery } from '@apollo/client'

import CategoryPreview from '../../components/category-preview/category-preview.component'
import Spinner from '../../components/spinner/spinner.component'
import { GET_CATEGORIES } from '../../gql-operations/queries/categories'

const CategoriesPreview = () => {
    const { loading, data } = useQuery(GET_CATEGORIES)

    return (
        <>
            {loading ? (
                <Spinner />
            ) : (
                data?.categories.map(({ title, items }) => {
                    return (
                        <CategoryPreview
                            key={title}
                            title={title}
                            items={items}
                        />
                    )
                })
            )}
        </>
    )
}

export default CategoriesPreview
