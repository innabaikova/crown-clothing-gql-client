import { Fragment } from 'react'
import { Outlet } from 'react-router-dom'
import CartIcon from '../../components/cart-icon/cart-icon.component'
import CartDropdown from '../../components/cart-dropdown/cart-dropdown.component'
import { ReactComponent as CrwnLogo } from '../../assets/crown.svg'
import { signOutUser } from '../../utils/firebase/firebase.utils'

import {
    NavigationContainer,
    NavLinks,
    NavLink,
    LogoContainer,
} from './navigation.styles'
import { useQuery, useReactiveVar } from '@apollo/client'
import { currentUser, isCartOpen } from '../../cache'
import { GET_CART } from '../../gql-operations/queries/cart'

const Navigation = () => {
    const isAuthenticated = useReactiveVar(currentUser)
    const showCart = useReactiveVar(isCartOpen)
    const { data, error } = useQuery(GET_CART)
    const cart = error ? null : data?.cart

    return (
        <Fragment>
            <NavigationContainer>
                <LogoContainer to="/">
                    <CrwnLogo className="logo" />
                </LogoContainer>
                <NavLinks>
                    <NavLink to="/shop">SHOP</NavLink>

                    {isAuthenticated ? (
                        <NavLink as="span" onClick={signOutUser}>
                            SIGN OUT
                        </NavLink>
                    ) : (
                        <NavLink to="/auth">SIGN IN</NavLink>
                    )}
                    <CartIcon cart={cart} isCartOpen={showCart}/>
                </NavLinks>
                {showCart && <CartDropdown cart={cart} />}
            </NavigationContainer>
            <Outlet />
        </Fragment>
    )
}

export default Navigation
