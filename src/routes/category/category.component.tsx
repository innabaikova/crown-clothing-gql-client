import { useQuery } from '@apollo/client'
import { Fragment } from 'react'
import { useParams } from 'react-router-dom'

import ProductCard from '../../components/product-card/product-card.component'
import Spinner from '../../components/spinner/spinner.component'
import { GET_CATEGORY } from '../../gql-operations/queries/categories'

import { CategoryContainer, Title } from './category.styles'

type CategoryRouteParams = { category: string }

const Category = () => {
    const { category } = useParams<CategoryRouteParams>()
    const { loading, data } = useQuery(GET_CATEGORY, {
        variables: { categoryId: category! },
    })

    return (
        <Fragment>
            <Title>{category!.toUpperCase()}</Title>
            {loading ? (
                <Spinner />
            ) : (
                <CategoryContainer>
                    {data?.category?.items.map(product => (
                        <ProductCard key={product.id} product={product} categoryId={category!}/>
                    ))}
                </CategoryContainer>
            )}
        </Fragment>
    )
}

export default Category
