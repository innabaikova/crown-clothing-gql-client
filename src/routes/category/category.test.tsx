import { MockedProvider, MockedResponse } from '@apollo/client/testing'
import { render, screen } from '@testing-library/react'
import { GET_CATEGORY } from '../../gql-operations/queries/categories'
import * as router from 'react-router'
import Category from './category.component'
import { GetCategoryQuery, Product } from '../../gql/graphql'

const mockItems: Product[] = [
    {
        id: '18',
        name: 'Black Jean Shearling',
        price: 125,
        imageUrl: 'https://i.ibb.co/XzcwL5s/black-shearling.png',
    },
]
const categoryId = 'jackets'
const mocks: MockedResponse<GetCategoryQuery>[] = [
    {
        request: {
            query: GET_CATEGORY,
            variables: {
                categoryId,
            },
        },
        result: {
            data: {
                category: {
                    id: categoryId,
                    items: mockItems
                },
            },
        },
    },
]

beforeEach(() => {
    jest.spyOn(router, 'useParams').mockImplementation(() => ({
        category: categoryId,
    }))
})

test('should render products in category', async () => {
    render(
        <MockedProvider mocks={mocks} addTypename={false}>
            <Category />
        </MockedProvider>
    )
    expect(screen.getByTestId('loading-spinner')).toBeVisible()
    expect(await screen.findByText(mockItems[0].name)).toBeVisible()
})
