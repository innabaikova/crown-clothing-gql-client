import { MockedProvider, MockedResponse } from '@apollo/client/testing'
import { render, screen } from '@testing-library/react'
import { GET_CART } from '../../gql-operations/queries/cart'
import { CartDataFragment, CartQuery } from '../../gql/graphql'
import { cart } from '../../utils/mocks/cart.mock'
import Checkout from './checkout.component'

const getMockedResponse = (cart: CartDataFragment): MockedResponse<CartQuery>[] => {
    return [
        {
            request: {
                query: GET_CART,
            },
            result: {
                data: { cart },
            },
        },
    ]
}

jest.mock('../../components/payment-form/payment-form.component', () => () => (
    <div>Payment Form Mock</div>
))

test('should render checkout page', async () => {
    const mocks = getMockedResponse(cart)
    render(
        <MockedProvider mocks={mocks} addTypename={true}>
            <Checkout />
        </MockedProvider>
    )
    expect(screen.getByTestId('loading-spinner')).toBeVisible()
    expect(await screen.findByText(cart.items[0].name)).toBeVisible()
})
