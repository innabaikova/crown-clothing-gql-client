import { useQuery } from '@apollo/client'
import { useEffect, useMemo } from 'react'
import { isCartOpen } from '../../cache'

import CheckoutItem from '../../components/checkout-item/checkout-item.component'
import PaymentForm from '../../components/payment-form/payment-form.component'
import Spinner from '../../components/spinner/spinner.component'
import { getFragment } from '../../gql'
import { CART_DATA } from '../../gql-operations/fragments/cart'
import { GET_CART } from '../../gql-operations/queries/cart'

import {
    CheckoutContainer,
    CheckoutHeader,
    HeaderBlock,
    Total,
} from './checkout.styles'

const Checkout = () => {
    const { loading, data } = useQuery(GET_CART)
    const cart = getFragment(CART_DATA, data?.cart)
    useEffect(() => {
        isCartOpen(false)
    }, [])

    return (
        <>
            {loading && <Spinner />}
            {cart && (
                <CheckoutContainer>
                    <CheckoutHeader>
                        <HeaderBlock>
                            <span>Product</span>
                        </HeaderBlock>
                        <HeaderBlock>
                            <span>Description</span>
                        </HeaderBlock>
                        <HeaderBlock>
                            <span>Quantity</span>
                        </HeaderBlock>
                        <HeaderBlock>
                            <span>Price</span>
                        </HeaderBlock>
                        <HeaderBlock>
                            <span>Remove</span>
                        </HeaderBlock>
                    </CheckoutHeader>
                    {cart.items.map((cartItem) => {
                        return <CheckoutItem key={cartItem.id} cartItem={cartItem} />
                    })}
                    <Total>Total: ${cart.total.price || 0}</Total>
                    <PaymentForm amount={cart.total.price || 0} />
                </CheckoutContainer>
            )}
        </>
    )
}

export default Checkout
