import { SpinnerOverlay, SpinnerContainer } from './spinner.styles';

const Spinner = () => (
  <SpinnerOverlay data-testid="loading-spinner">
    <SpinnerContainer />
  </SpinnerOverlay>
);

export default Spinner;
