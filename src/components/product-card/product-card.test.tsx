import { fireEvent, render, screen, waitFor } from '@testing-library/react'
import ProductCard from './product-card.component'
import { MockedProvider, MockedResponse } from '@apollo/client/testing'
import { AddToCartMutation, Product } from '../../gql/graphql'
import { ADD_TO_CART } from '../../gql-operations/mutations/cart'
import { cart } from '../../utils/mocks/cart.mock'

describe('ProductCard Component', () => {
    const product: Product = {
        name: 'test product',
        price: 100,
        imageUrl: 'https://via.placeholder.com/150',
        id: '1',
    }

    const categoryId = 't-shirts'

    let addToCartMutationCalled = false
    const mocks: MockedResponse<AddToCartMutation>[] = [
        {
            request: {
                query: ADD_TO_CART,
                variables: { productId: '1', categoryId },
            },
            result: () => {
                addToCartMutationCalled = true
                return { data: { addToCart: { ...cart } } }
            },
        },
    ]

    it('renders product card with correct details', async () => {
        render(
            <MockedProvider mocks={[]}>
                <ProductCard product={product} categoryId={categoryId} />
            </MockedProvider>
        )
        expect(screen.getByText(product.name)).toBeInTheDocument()
        expect(screen.getByText(product.price)).toBeInTheDocument()
    })

    it('calls addToCart mutation on button click', async () => {
        render(
            <MockedProvider mocks={mocks}>
                <ProductCard product={product} categoryId={categoryId} />
            </MockedProvider>
        )

        const addButton = screen.getByText('Add to cart')
        fireEvent.click(addButton)
        
        await waitFor(() => expect(addToCartMutationCalled).toBe(true))
    })
})
