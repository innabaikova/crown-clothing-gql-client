import { useMutation } from '@apollo/client'
import { FC } from 'react'
import { Product } from '../../gql/graphql'
import { ADD_TO_CART } from '../../gql-operations/mutations/cart'
import Button, { BUTTON_TYPE_CLASSES } from '../button/button.component'

import {
    ProductCartContainer,
    Footer,
    Name,
    Price,
} from './product-card.styles'
import { updateCart } from '../../gql-operations/helpers/update-cart'

type ProductCardProps = {
    product: Product
    categoryId: string
}

const ProductCard: FC<ProductCardProps> = ({ product, categoryId }) => {
    const { name, price, imageUrl, id: productId } = product
    const [addToCart] = useMutation(ADD_TO_CART, {
        variables: { productId, categoryId },
        update: (cache, { data }) => data && updateCart(cache, data.addToCart)
    })

    return (
        <ProductCartContainer>
            <img src={imageUrl} alt={`${name}`} />
            <Footer>
                <Name>{name}</Name>
                <Price>{price}</Price>
            </Footer>
            <Button
                buttonType={BUTTON_TYPE_CLASSES.inverted}
                onClick={() => addToCart()}
            >
                Add to cart
            </Button>
        </ProductCartContainer>
    )
}

export default ProductCard
