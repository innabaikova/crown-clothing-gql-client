import { FC, FormEvent, useState } from 'react'
import { CardElement, useStripe, useElements } from '@stripe/react-stripe-js'
import { FormContainer } from './payment-form.styles'
import { BUTTON_TYPE_CLASSES } from '../button/button.component'
import { PaymentButton, PaymentFormContainer } from './payment-form.styles'
import { StripeCardElement } from '@stripe/stripe-js'
import { useMutation, useReactiveVar } from '@apollo/client'
import { currentUser } from '../../cache'
import { CLEAR_CART } from '../../gql-operations/mutations/cart'
import { useNavigate } from 'react-router-dom'
import { updateCart } from '../../gql-operations/helpers/update-cart'

type PaymentFormProps = {
    amount: number
}

const isValidCardElement = (
    card: StripeCardElement | null
): card is StripeCardElement => card !== null

const PaymentForm: FC<PaymentFormProps> = ({ amount }) => {
    const stripe = useStripe()
    const elements = useElements()
    const user = useReactiveVar(currentUser)
    const [isProcessingPayment, setIsProcessingPayment] = useState(false)
    
    const [clearCart] = useMutation(CLEAR_CART, {
        update: (cache, { data }) => data && updateCart(cache, data.clearCart),
    })
    const navigate = useNavigate()

    const paymentHandler = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        if (!stripe || !elements) {
            return
        }
        setIsProcessingPayment(true)
        const response = await fetch(
            '/.netlify/functions/create-payment-intent',
            {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ amount: amount * 100 }),
            }
        ).then(res => {
            return res.json()
        })

        const clientSecret = response.paymentIntent.client_secret

        const card = elements.getElement(CardElement)

        if (!isValidCardElement(card)) return

        const paymentResult = await stripe.confirmCardPayment(clientSecret, {
            payment_method: {
                card,
                billing_details: {
                    name: user ? user.displayName : 'Guest',
                },
            },
        })

        setIsProcessingPayment(false)

        if (paymentResult.error) {
            alert(paymentResult.error.message)
        } else {
            if (paymentResult.paymentIntent.status === 'succeeded') {
                alert('Payment Successful!')
                clearCart()
                navigate('/shop')
            }
        }
    }

    return (
        <PaymentFormContainer>
            <FormContainer onSubmit={paymentHandler}>
                <h2>Credit Card Payment:</h2>
                <CardElement />
                <PaymentButton
                    buttonType={BUTTON_TYPE_CLASSES.inverted}
                    isLoading={isProcessingPayment}
                >
                    Pay Now
                </PaymentButton>
            </FormContainer>
        </PaymentFormContainer>
    )
}
export default PaymentForm
