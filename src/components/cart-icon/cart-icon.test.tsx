import { render, fireEvent } from '@testing-library/react'
import { makeFragmentData } from '../../gql'
import { CART_DATA } from '../../gql-operations/fragments/cart'
import { CartDataFragment } from '../../gql/graphql'
import CartIcon from './cart-icon.component'
import * as cache from '../../cache'

describe('CartIcon', () => {
    const cartData: CartDataFragment = {
        items: [],
        total: {
            quantity: 2,
            price: 115,
        },
    }
    const mockCart = makeFragmentData(cartData, CART_DATA)

    it('displays the correct item count', () => {
        const { getByTestId } = render(
            <CartIcon cart={mockCart} isCartOpen={false} />
        )
        expect(getByTestId('item-count').textContent).toBe(
            cartData.total.quantity.toString()
        )
    })

    it('displays zero if cart is undefined', () => {
        const { getByTestId } = render(
            <CartIcon cart={undefined} isCartOpen={false} />
        )
        expect(getByTestId('item-count').textContent).toBe('0') 
    })

    it('calls the toggleIsCartOpen function when clicked', () => {
        const mockIsCartOpen = jest.fn(() => false)
        jest.spyOn(cache, 'isCartOpen').mockImplementation(mockIsCartOpen)

        const { getByTestId } = render(
            <CartIcon cart={mockCart} isCartOpen={false} />
        )
        fireEvent.click(getByTestId('item-count'))
        expect(mockIsCartOpen).toHaveBeenCalledWith(true)
    })
})
