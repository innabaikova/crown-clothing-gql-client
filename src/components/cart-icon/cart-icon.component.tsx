import { ReactComponent as ShoppingIcon } from '../../assets/shopping-bag.svg';

import { CartIconContainer, ItemCount } from './cart-icon.styles';
import { isCartOpen } from '../../cache';
import { FragmentType, getFragment } from '../../gql';
import { CART_DATA } from '../../gql-operations/fragments/cart'

type CartIconProps = {
  cart?: FragmentType<typeof CART_DATA> | null,
  isCartOpen: boolean
}

const CartIcon = (props: CartIconProps) => {
  const cart = getFragment(CART_DATA, props.cart)
  const toggleIsCartOpen = ()=> isCartOpen(!props.isCartOpen)

  return (
    <CartIconContainer onClick={toggleIsCartOpen}>
      <ShoppingIcon className='shopping-icon' />
      <ItemCount data-testid='item-count'>{cart?.total.quantity || 0}</ItemCount>
    </CartIconContainer>
  );
};

export default CartIcon;
