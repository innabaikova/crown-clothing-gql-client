import { render } from '@testing-library/react'
import CartItem from './cart-item.component'

describe('CartItem', () => {
    const cartItem = {
        id: '1',
        name: 'Test Item',
        imageUrl: 'test-image-url',
        price: 10,
        quantity: 3,
    }

    it('renders the cart item image, name, price, and quantity', () => {
        const { getByAltText, getByText } = render(
            <CartItem cartItem={cartItem} />
        )

        expect(getByAltText(cartItem.name)).toBeInTheDocument()
        expect(getByText(cartItem.name)).toBeInTheDocument()
        expect(getByText(`${cartItem.quantity} x $${cartItem.price}`)).toBeInTheDocument()
    })
})
