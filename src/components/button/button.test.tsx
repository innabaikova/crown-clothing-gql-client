import { render, screen } from "@testing-library/react"
import Button from "./button.component"

test('should render a base button', () => {
    const buttonTitle = 'Click me if you dare'
    render(<Button>{buttonTitle}</Button>)
    const button = screen.getByText(buttonTitle)

    expect(button).toBeInTheDocument()
})

test('should render a spinner in loading state', () => {
    render(<Button isLoading={true}>{'Say Hi'}</Button>)
    const button = screen.getByRole('button')

    expect(button).not.toHaveTextContent('Say Hi')
})

test('should show spinner in loading state', () => {
    render(<Button isLoading={true}>{'Bla bla'}</Button>)

    expect(screen.getByTestId('button-spinner')).toBeVisible()
})