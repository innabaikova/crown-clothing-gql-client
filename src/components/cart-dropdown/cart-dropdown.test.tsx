import { fireEvent, render, screen } from '@testing-library/react'
import CartDropdown from './cart-dropdown.component'
import * as router from 'react-router'
import { CartDataFragment } from '../../gql/graphql'
import { makeFragmentData } from '../../gql'
import { CART_DATA } from '../../gql-operations/fragments/cart'

const cartData: CartDataFragment = {
    items: [
        {
            id: '20',
            name: 'Grey Jean Jacket',
            price: 90,
            imageUrl: 'https://i.ibb.co/N71k1ML/grey-jean-jacket.png',
            quantity: 1,
        },
        {
            id: '1',
            name: 'Brown Brim',
            price: 25,
            imageUrl: 'https://i.ibb.co/ZYW3VTp/brown-brim.png',
            quantity: 1,
        },
    ],
    total: {
        quantity: 2,
        price: 115,
    },
}

const cart = makeFragmentData(cartData, CART_DATA)

const navigate = jest.fn()

beforeEach(() => {
    jest.spyOn(router, 'useNavigate').mockImplementation(() => navigate)
})

test('should render a cart dropdown', async () => {
    render(<CartDropdown cart={cart} />)
    expect(await screen.findByText(cartData.items[0].name)).toBeVisible()
})

test('should navigate to checkout when GO TO CHECKOUT has been clicked', async () => {
    render(<CartDropdown cart={cart} />)

    const button = await screen.findByRole('button')//screen.findByText('GO TO CHECKOUT')
    fireEvent.click(button)
    expect(navigate).toHaveBeenCalledWith('/checkout')
})

test('should show empty message if cart is empty', async () => {
    const emptyCartData: CartDataFragment = {
        items: [],
        total: {
            quantity: 0,
            price: 0,
        },
    }
    const emptyCart = makeFragmentData(emptyCartData, CART_DATA)
    render(<CartDropdown cart={emptyCart} />)

    expect(await screen.findByText('Your cart is empty')).toBeVisible()
})
