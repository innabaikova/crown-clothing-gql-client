import { useNavigate } from 'react-router-dom'

import Button from '../button/button.component'
import CartItem from '../cart-item/cart-item.component'

import {
    CartDropdownContainer,
    EmptyMessage,
    CartItems,
} from './cart-dropdown.styles'
import { useCallback } from 'react'
import { FragmentType, getFragment } from '../../gql'
import { CART_DATA } from '../../gql-operations/fragments/cart'

type CartDropdownProps = {
    cart?: FragmentType<typeof CART_DATA> | null
}

const CartDropdown = (props: CartDropdownProps) => {
    const cart = getFragment(CART_DATA, props.cart)
    const navigate = useNavigate()

    const goToCheckoutHandler = useCallback(() => {
        navigate('/checkout')
    }, [])

    return (
        <CartDropdownContainer>
            <CartItems>
                {cart?.items.length ? (
                    cart.items.map(item => (
                        <CartItem key={item.id} cartItem={item} />
                    ))
                ) : (
                    <EmptyMessage>Your cart is empty</EmptyMessage>
                )}
            </CartItems>
            <Button onClick={goToCheckoutHandler}>GO TO CHECKOUT</Button>
        </CartDropdownContainer>
    )
}

export default CartDropdown
