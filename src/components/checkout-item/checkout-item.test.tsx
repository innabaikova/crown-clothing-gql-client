import { render, fireEvent, waitFor } from '@testing-library/react'
import { MockedProvider, MockedResponse } from '@apollo/client/testing'
import CheckoutItem from './checkout-item.component'
import {
    AddToCartMutation,
    CartItem,
    ClearCartItemMutation,
    RemoveFromCartMutation,
} from '../../gql/graphql'
import { ADD_TO_CART, CLEAR_ITEM, REMOVE_ITEM } from '../../gql-operations/mutations/cart'
import { cart } from '../../utils/mocks/cart.mock'

const mockCartItem: CartItem = {
    id: '1',
    name: 'Test Item',
    imageUrl: 'test-image-url',
    price: 99.99,
    quantity: 2,
}

describe('CheckoutItem Component', () => {
    it('renders with correct data', () => {
        const { getByText, getByAltText } = render(
            <MockedProvider mocks={[]} addTypename={false}>
                <CheckoutItem cartItem={mockCartItem} />
            </MockedProvider>
        )

        expect(getByText(mockCartItem.name)).toBeInTheDocument()
        expect(getByAltText(mockCartItem.name)).toHaveAttribute(
            'src',
            mockCartItem.imageUrl
        )
        expect(getByText(`${mockCartItem.price}`)).toBeInTheDocument()
        expect(getByText(`${mockCartItem.quantity}`)).toBeInTheDocument()
    })

    it('calls RemoveFromCartMutation when decrease quantity arrow is clicked', async () => {
        let removeFromCartMutationCalled = false
        const mocks: MockedResponse<RemoveFromCartMutation>[] = [
            {
                request: {
                    query: REMOVE_ITEM,
                    variables: { productId: '1' },
                },
                result: () => {
                    removeFromCartMutationCalled = true
                    return { data: { removeFromCart: { ...cart } } }
                },
            },
        ]
        const { getByTestId } = render(
            <MockedProvider mocks={mocks} addTypename={false}>
                <CheckoutItem cartItem={mockCartItem} />
            </MockedProvider>
        )

        fireEvent.click(getByTestId('decrease-quantity-arrow'))

        await waitFor(() => expect(removeFromCartMutationCalled).toBe(true))
    })

    it('calls AddToCartMutation when increase quantity arrow is clicked', async () => {
        let addToCartMutationCalled = false
        const mocks: MockedResponse<AddToCartMutation>[] = [
            {
                request: {
                    query: ADD_TO_CART,
                    variables: { productId: '1' },
                },
                result: () => {
                    addToCartMutationCalled = true
                    return { data: { addToCart: { ...cart } } }
                },
            },
        ]
        const { getByTestId } = render(
            <MockedProvider mocks={mocks} addTypename={false}>
                <CheckoutItem cartItem={mockCartItem} />
            </MockedProvider>
        )

        fireEvent.click(getByTestId('increase-quantity-arrow'))

        await waitFor(() => expect(addToCartMutationCalled).toBe(true))
    })

    it('calls ClearCartItemMutation when remove button is clicked', async () => {
        let clearCartItemMutationCalled = false
        const mocks: MockedResponse<ClearCartItemMutation>[] = [
            {
                request: {
                    query: CLEAR_ITEM,
                    variables: { productId: '1' },
                },
                result: () => {
                    clearCartItemMutationCalled = true
                    return { data: { clearCartItem: { ...cart } } }
                },
            },
        ]
        const { getByTestId } = render(
            <MockedProvider mocks={mocks} addTypename={false}>
                <CheckoutItem cartItem={mockCartItem} />
            </MockedProvider>
        )

        fireEvent.click(getByTestId('remove-item-button'))

        await waitFor(() => expect(clearCartItemMutationCalled).toBe(true))
    })
})
