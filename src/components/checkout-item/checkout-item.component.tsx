import { useMutation } from '@apollo/client'
import { FC } from 'react'
import { CartItem } from '../../gql/graphql'
import { ADD_TO_CART, CLEAR_ITEM, REMOVE_ITEM } from '../../gql-operations/mutations/cart'
import {
    CheckoutItemContainer,
    ImageContainer,
    BaseSpan,
    Quantity,
    Arrow,
    Value,
    RemoveButton,
} from './checkout-item.styles'
import { updateCart } from '../../gql-operations/helpers/update-cart'

type CheckoutItemProps = {
    cartItem: CartItem
}

const CheckoutItem: FC<CheckoutItemProps> = ({ cartItem }) => {
    const { name, imageUrl, price, quantity } = cartItem
    const variables = { productId: cartItem.id }
    const [removeItem] = useMutation(REMOVE_ITEM, {
        variables,
        update: (cache, { data }) => data && updateCart(cache, data.removeFromCart)
    })
    const [addToCart] = useMutation(ADD_TO_CART, {
        variables,
        update: (cache, { data }) => data && updateCart(cache, data.addToCart)
    })
    const [clearItem] = useMutation(CLEAR_ITEM, {
        variables,
        update: (cache, { data }) => data && updateCart(cache, data.clearCartItem)
    })

    return (
        <CheckoutItemContainer>
            <ImageContainer>
                <img src={imageUrl} alt={`${name}`} />
            </ImageContainer>
            <BaseSpan> {name} </BaseSpan>
            <Quantity>
                <Arrow onClick={() => removeItem()} data-testid="decrease-quantity-arrow">&#10094;</Arrow>
                <Value>{quantity}</Value>
                <Arrow onClick={() => addToCart()} data-testid="increase-quantity-arrow">&#10095;</Arrow>
            </Quantity>
            <BaseSpan> {price}</BaseSpan>
            <RemoveButton onClick={() => clearItem()} data-testid="remove-item-button">&#10005;</RemoveButton>
        </CheckoutItemContainer>
    )
}

export default CheckoutItem
