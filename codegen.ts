import { CodegenConfig } from '@graphql-codegen/cli'

const config: CodegenConfig = {
    schema: 'http://localhost:5555/graphql',
    documents: ['src/gql-operations/**/*.ts', '!src/gql/**/*'],
    generates: {
        './src/gql/': {
            preset: 'client',
            plugins: [],
            presetConfig: {
                fragmentMasking: {
                    unmaskFunctionName: 'getFragment',
                },
            },
        },
    },
}

export default config
